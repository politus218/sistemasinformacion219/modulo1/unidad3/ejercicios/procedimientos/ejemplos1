﻿DROP DATABASE IF EXISTS programacion;
CREATE DATABASE programacion;
USE programacion;

-- Ejemplos de procedimientos almacenados

  -- Procedimiento 1 que muestre la fecha de hoy.

  DELIMITER //
  CREATE OR REPLACE PROCEDURE procedimiento1()
    BEGIN
      SELECT NOW();
    END //
  DELIMITER ;

CALL procedimiento1();

-- Crear una variable de tipo datetime Y almacenar en esa variable la fecha de hoy. Y que nos muestre esa variable.

DELIMITER //
CREATE OR REPLACE PROCEDURE procedim2()
  BEGIN
-- declarar
  DECLARE v datetime;
-- introducir (input)
  SET v=now();
-- output (sacar)
  SELECT v;
  END //
DELIMITER ;

CALL procedim2();


-- Procedimiento 3  Proced3 suma 2 números que recibe como argumentos y me muestre la suma.

  DELIMITER //
  CREATE OR REPLACE PROCEDURE Proced3(num1 int,num2 int)
    BEGIN
      DECLARE suma int DEFAULT 0;
      SET suma=num1+num2;
      SELECT suma;
    END //
  DELIMITER ;

  CALL Proced3(1,5);
  CALL Proced3(88,2);

-- Restar 2 nºs que recibe como argumentos 

  DELIMITER //
    CREATE OR REPLACE PROCEDURE P4(num1 int, num2 int)
      BEGIN
        DECLARE resultado int DEFAULT 0;
        SET resultado=num1-num2;
        SELECT resultado;
      END //
    DELIMITER ;

    CALL P4(1,5);

-- Procedimiento que sume 2 números y los almacene en una tabla llamada datos. La tabla la debe crear en caso de que no exista.

  DELIMITER //
    CREATE OR REPLACE PROCEDURE P5(num1 int, num2 int)
        BEGIN
      -- declarar e inicializar las variables
      DECLARE resultado int DEFAULT 0;
      -- crear la tabla
          CREATE TABLE IF NOT EXISTS datos(
            id int AUTO_INCREMENT,
            n1 int DEFAULT 0,
            n2 int DEFAULT 0,  
            suma int DEFAULT 0,
            PRIMARY KEY(id)
                                        );
      -- realizar los cálculos
      SET resultado =num1+num2;
      -- almacenar el resultado en la tabla.
      INSERT INTO datos VALUES(DEFAULT, num1,num2, resultado);
      END //
    DELIMITER ;

    CALL P5(14,5);
    SELECT * FROM datos;

-- p6 suma, producto 2 números call p6(14,5) Crear una tabla llamada sumaproducto(id,d1,d2,suma, producto)
-- Introducir los datos en la tabla y los resultados.
  
  DELIMITER //
    CREATE OR REPLACE PROCEDURE p6(num1 int, num2 int)
      BEGIN
        DECLARE suma int DEFAULT 0;
        DECLARE producto int DEFAULT 0;
          CREATE TABLE IF NOT EXISTS sumaproducto(
            id int AUTO_INCREMENT,
            d1 int DEFAULT 0,
            d2 int DEFAULT 0,
            suma int DEFAULT 0,
            producto int DEFAULT 0,
            PRIMARY KEY (id));
        SET suma = num1+num2;
        SET producto = num1*num2;
      INSERT INTO sumaproducto VALUES(DEFAULT, num1,num2, suma,producto);
      END //
    DELIMITER ; 

CALL p6(4,5);
SELECT * FROM sumaproducto;

DROP PROCEDURE IF EXISTS cociente;
CREATE PROCEDURE cociente(dividendo int, divisor int)
  BEGIN
    IF divisor<>0 THEN
      SELECT dividendo/divisor;
    ELSE
      SELECT 'infinito';
    END IF;
  END;

  CALL cociente(3,1);


-- p7 Objetivo: elevar un nº a otro. Almacenar el resultado y los números en la tabla 
-- potencia. Argumentos: base y el exponente call p7(2,3) el resultado será 8.

  DELIMITER //
    CREATE OR REPLACE PROCEDURE p7 (base int, exponente int)
      BEGIN
        DECLARE resultado int DEFAULT 0;
       SET resultado = POW (base, exponente);  
        CREATE TABLE IF NOT EXISTS potencia (
          id int AUTO_INCREMENT,
          n1 int DEFAULT 0,
          n2 int DEFAULT 0,
          potencia int DEFAULT 0,
          PRIMARY KEY (id) );
                                            
      INSERT INTO potencia (n1,n2,potencia)
      VALUES (base, exponente, resultado);
           
      END //
    DELIMITER ;
CALL p7(2,3);
SELECT * FROM potencia;

-- p8. Objetivo: realizar la raiz cuadrada de un nº.
-- Almacenar el nº y su raiz en la tabla raiz.

  DELIMITER //
    CREATE OR REPLACE PROCEDURE p8 (n1 int)
      BEGIN

        DECLARE resultado float DEFAULT 0;
      SET resultado = SQRT(n1); 
        CREATE TABLE IF NOT EXISTS raizcuadrada(
        id int AUTO_INCREMENT,
        n1 int DEFAULT 0,
        raizcuadrada float DEFAULT 0,
        PRIMARY KEY (id) );

      INSERT INTO raizcuadrada (n1,raizcuadrada)
      VALUES (n1, resultado);

      END //
    DELIMITER ;
CALL p8(2);
SELECT * FROM raizcuadrada;

-- Analizar el siguiente procedimiento y añadir
-- lo necesario para que de la longitud del texto y lo almacene en la tabla en un campo
-- llamado longitud.
-- Crea una tabla con un campo llamado longitud.
  DELIMITER //
    DROP PROCEDURE IF EXISTS p9 //
    CREATE PROCEDURE p9(t varchar(50))
      BEGIN
        DECLARE longitud int DEFAULT 0;
      SET longitud = CHAR_LENGTH(t);
      CREATE TABLE IF NOT EXISTS texto(
        id int AUTO_INCREMENT PRIMARY KEY,
        texto varchar(50),
        longitud int
      );
      INSERT INTO texto(texto,longitud) 
          VALUE (t, longitud);    
      END //
    DELIMITER ;
CALL p9("papiroflexia");
SELECT * FROM texto;

-- sintaxis básica del if
-- ejemplo procedimiento 10, p10:
-- Procedimiento al cual le paso un número y me indica si es mayor que 100.

--  select if (nº>100, "más grande que 100","más pequeño que 100");

DELIMITER //
  CREATE OR REPLACE PROCEDURE p10(num int)
    BEGIN
    IF (num>100) THEN
    SELECT "es más grande que 100";
    END IF;
    END //
  DELIMITER ;
CALL p10(120);

-- La misma con la instrucción else para que nos saque también algo si es más pequeño.

DELIMITER //
  CREATE OR REPLACE PROCEDURE p10(num int)
    BEGIN
    IF (num>100) THEN
    SELECT "es más grande que 100";
  ELSE
    SELECT "es más pequeño que 100";
  END IF;
    END //
  DELIMITER ;
CALL p10(90);

-- p11 Procedimiento al cual le paso un nº y me indica si es mayor, igual o menor que
--  100.
  DELIMITER //
    CREATE OR REPLACE PROCEDURE p11(num int)
    BEGIN
      IF (num>100) THEN
        SELECT "es mayor que 100";
          ELSEIF (num<100) THEN
            SELECT "es más pequeño que 100";
              ELSEIF (num=100) THEN 
                 SELECT "es igual que 100";
      END IF;
    END //
  DELIMITER ;
CALL p11(100.4);


--Mejor programado aunque igual resultado.

DELIMITER //
    CREATE OR REPLACE PROCEDURE prod12(num int)
    BEGIN
    DECLARE resultado varchar(50) DEFAULT "igual a 100";
      IF (num>100) THEN
        SET resultado= "mayor que 100";
        ELSEIF (num<100) THEN
        set resultado= "es más pequeño que 100";       
      END IF;
      SELECT resultado;
    END //
  DELIMITER ;
CALL prod12(99.5);

-- p12. Crear un procedimiento que le pasas una nota y te debe devolver lo siguiente
-- Muy deficiente: si la nota es menor que 2.5.
-- Suspenso: si la nota está entre 2.5 y 5.
-- Suficiente: si la nota es entre 5 y 6.
-- Bien: si la nota es entre 6 y 7.
-- Notable: si la nota está entre 7 y 9.
-- Sobresaliente: si la nota está entre 9 y 10.

DELIMITER //
  CREATE OR REPLACE PROCEDURE p12(n1 float)
    BEGIN
      DECLARE resultado varchar(50) DEFAULT "sin calificar";
        IF (n1<2.5) THEN
          SET resultado= "0 al cociente, has sacado un muy deficiente";
          ELSEIF (n1>=2.5 AND n1<5) THEN
          SET resultado= "Trabaja más la mente, has sacado insuficiente";
          ELSEIF (n1>=5 AND n1<6) THEN
          SET resultado= "Vamos a ser indulgente, has sacado suficiente";
          ELSEIF (n1>=6 AND n1<7) THEN
          SET resultado= "Además de elegante eres listo también, has sacado un bien";
          ELSEIF (n1>=7 AND n1<9) THEN
          SET resultado="Eres una persona honorable, has sacado notable";
          ELSEIF (n1>=9) THEN
          SET resultado="Eres brillante, la nota no miente, has sacado sobresaliente";
        END IF;
        select resultado;
       SELECT resultado; 
    END //
  DELIMITER ;
call p12(4.3);

-- p13. El mismo ejercicio que el anterior pero modificado para case.

DELIMITER //
  CREATE OR REPLACE PROCEDURE p13(n1 float)
    BEGIN
      DECLARE resultado varchar(50) DEFAULT "sin calificar";
        CASE
          WHEN (n1<2.5) THEN
          SET resultado= "0 al cociente, has sacado un muy deficiente";
          WHEN (n1>=2.5 AND n1<5) THEN
          SET resultado= "Trabaja más la mente, has sacado insuficiente";
          WHEN (n1>=5 AND n1<6) THEN
          SET resultado= "Vamos a ser indulgente, has sacado suficiente";
          WHEN (n1>=6 AND n1<7) THEN
          SET resultado= "Además de elegante eres listo también, has sacado un bien";
          WHEN (n1>=7 AND n1<9) THEN
          SET resultado="Eres una persona honorable, has sacado notable";
          WHEN (n1>=9) THEN
          SET resultado="Eres brillante, la nota no miente, has sacado sobresaliente";
        END CASE;
      SELECT resultado; 
    END //
  DELIMITER ;
call p13(8.2);

-- Argumentos de entrada / salida.
-- p14. Calcular la suma de 2 números. 
-- La suma la debe almacenar en un argumento de salida llamada resultado.
-- CALL p14(n1 int, n2 int, OUT resultado int)

  DELIMITER //
    CREATE OR REPLACE PROCEDURE p14(n1 int, n2 int, OUT resultado int)
      BEGIN
        SET resultado=n1+n2;
      END //
    DELIMITER ;

SET @suma=0;
CALL p14(2,5,@suma);
SELECT @suma

-- p15. Procedimiento que reciba como argumento un nº n1 y otro nº n2 
-- y haga una suma y un producto resultado(argumento de salida o entrada/ salida.
--  y cálculo (texto argumento de entrada).
-- Si cálculo vale suma entonces resultado tendrá la suma de n1 + n2
-- Si cálculo vale producto entonces resultado tendrá el producto de n1*n2

DELIMITER //
  CREATE OR REPLACE PROCEDURE p15(n1 int, n2 int, INOUT resul int, operac varchar(50))
    BEGIN
      IF(operac="suma") THEN
        set resul=n1+n2;
      ELSEIF (operac="producto") THEN
        SET resul=n1*n2;
      END IF;
    END //
DELIMITER ;

set @resul=0;
CALL p15(7,2,@resul,"producto");
SELECT @resul;


-- p16. Usando while mostrar en pantalla nºs del 1 al 10.
        
DELIMITER //
  CREATE OR REPLACE PROCEDURE p16()
    BEGIN
      DECLARE conta int DEFAULT 1;
      WHILE (conta<=10) DO
      SELECT conta;
      set conta=conta+1 ;
      END WHILE;
    END //
  DELIMITER ;     
        
CALL p16();   
        
-- p17. Igual que el anterior pero metiendo los 10 números en una tabla temporal.
        
    DELIMITER //
    CREATE OR REPLACE PROCEDURE p17()
    BEGIN
      DECLARE conta int DEFAULT 1;
      
      CREATE TEMPORARY TABLE IF NOT EXISTS nums ( 
        num int
        );
      WHILE (conta<=10) DO                                
      INSERT INTO nums (num) VALUES (conta);
       
      set conta=conta+1 ;
      END WHILE;

      SELECT * FROM nums;  
      
    END //
  DELIMITER ;     
        
CALL p17();  
        
-- p18. Crear un procedimiento que le pasas un nº y te muestra desde el nº1 hasta el 
-- nº pasado, usar una tabla temporal. Hacerlo con While.

DELIMITER //
    CREATE OR REPLACE PROCEDURE p18(num1 int)
    BEGIN
      DECLARE conta int DEFAULT 1;
      
      CREATE TEMPORARY TABLE IF NOT EXISTS nums1 ( 
        num1 int
                                                );
      WHILE (conta<=num1) DO                                
      INSERT INTO nums1 (num1) VALUE (conta);
      set conta=conta+1;
      END WHILE;
      SELECT * FROM nums1;  
      
    END //
  DELIMITER ;     
        
CALL p18(5);

-- FUNCIONES.

-- F1. Le paso los nºs y me devuelve la suma de ellos.

DELIMITER //
CREATE OR REPLACE FUNCTION f1(n1 int, n2 int) 
returns int
BEGIN
  DECLARE resul int DEFAULT 0;
  set resul=n1+n2;
  RETURN resul;
END //
DELIMITER;

SELECT f1(5,6);

-- Disparadores.

CREATE OR REPLACE TABLE salas(
id int auto_increment PRIMARY KEY,
butacas int,
fecha date,
edad int DEFAULT 0
                             );

CREATE OR REPLACE TABLE ventas(
id int auto_increment PRIMARY KEY,
sala int,
num int DEFAULT 0             );

-- Salas. Controla la inserción de registros nuevos. 
-- Disparador que calcule la edad de la sala en función de su fecha de alta.

DELIMITER //
CREATE OR REPLACE TRIGGER salasBI
BEFORE INSERT 
ON salas
FOR EACH ROW
BEGIN 
set new.edad= TIMESTAMPDIFF(year, new.fecha, NOW());
END //
DELIMITER;

 -- Introducimos un registro para probar el disparador.

INSERT INTO salas(butacas, fecha)  VALUES (50, "2011/1/1");
SELECT * FROM salas;

-- Si NO tuvieramos disparador lo haríamos así:

-- UPDATE salas s

-- set new.edad= TIMESTAMPDIFF(year, new.fecha, NOW());
        
-- SalasBU:
-- Controla la actualización de registros
-- Disparador para que me calcule la edad de la sala en función de su fecha de alta

  DELIMITER //
CREATE OR REPLACE TRIGGER salasBU
BEFORE UPDATE 
ON salas
FOR EACH ROW
BEGIN 
set new.edad= TIMESTAMPDIFF(year, new.fecha, NOW());
END //
DELIMITER;     
        
INSERT INTO salas(butacas, fecha)  VALUES (50, "2011/08/29");
SELECT * FROM salas;    

-- Vamos a comprobar el funcionamiento de los 2 disparadores.                                                                               

INSERT INTO salas (butacas, fecha)
  VALUES (56 ,"2005/11/01"),(22, "1979/09/05");

SELECT * FROM salas;

UPDATE salas
  set fecha="1990/05/30";

SELECT * FROM salas;

 
-- Necesitamos que la tabla salas disponga de 3 campos nuevos.
-- Esos campos serán día, mes y año.

-- Esto sería con las instrucciones: ALTER TABLE.

-- Queremos que esos campos AUTOMATICAMENTE tengan el día, mes y año de la fecha.
-- Crear un disparador para la inserción y la actualización.

ALTER TABLE salas
  ADD COLUMN dia int,
  ADD COLUMN mes int,
  ADD COLUMN agno int;

DELIMITER //
CREATE OR REPLACE TRIGGER salasBI
BEFORE INSERT 
ON salas
FOR EACH ROW
BEGIN 
set new.edad= TIMESTAMPDIFF(year, new.fecha, NOW());
set new.dia=DAY(new.fecha);
set new.mes=MONTH(new.fecha);
set new.año=year(new.fecha);
END //
DELIMITER;

 DELIMITER //
CREATE OR REPLACE TRIGGER salasBU
BEFORE UPDATE 
ON salas
FOR EACH ROW
BEGIN 
set new.edad= TIMESTAMPDIFF(year, new.fecha, NOW());
set new.dia=DAY(new.fecha);
set new.mes=MONTH(new.fecha);
set new.año=year(new.fecha);
END //
DELIMITER;     